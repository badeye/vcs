package main;

import java.util.Scanner;

public class uzdScanner {
    public static void main(String[] args) {

        System.out.println("uzduotis 2");

        boolean check=false;
        while (!check){
            check=uzd2();
        }



        System.out.println("uzduotis 3");
        uzd3();
    }

    private static int readNumber(){
        Scanner scanner = new Scanner(System.in);
        int readNumber=scanner.nextInt();

        return readNumber;

    }

    private static void uzd3(){
        System.out.println("Iveskite skaiciu");
        int x=readNumber();

        if (x<0) System.out.println("Neigiamas");
        else if(x>100) System.out.println("Daugiau negu 100");
        else if(x>=40 && x<=60) System.out.println("Tarp 40 ir 60");
        else System.out.println("Kitas pranesimas :)");

    }

    private static boolean uzd2(){
        System.out.println("Iveskite amziu");
        int x=readNumber();
        if (x<0 || x>100) {
            System.out.println("Netikras amzius. Darom dar karta");
            return false;
        }

        if (x>=18) System.out.println("balsuoti galima");
        return true;

    }


}
