package main;

public class Student {
    double pazymiuVidurkis;
    String asmensKodas;

    public static int susumuok(int... b) {
        int sum = 0;
        for (int i : b) sum += i;
        return sum;
    }

    public void nustatytiVidurki(int... paz) {
        int size = paz.length;
        double sum = 0;
        for (int p : paz) sum += p;
        this.pazymiuVidurkis = sum / paz.length;
    }

    public void setAsmensKodas(String asmensKodas) {
        this.asmensKodas = asmensKodas;
    }

    public void show() {

//        if(pazymiuVidurkis>5.0 && pazymiuVidurkis<9.0)
//            System.out.println("Studento "+asmensKodas+" pazymiu vidurkis: "+pazymiuVidurkis);
//        else
//            System.out.println("Nerodysiu");

        System.out.println(pazymiuVidurkis > 5 ? ("Studento " + asmensKodas + " pazymiu vidurkis: " + pazymiuVidurkis) : "Nerodysiu");
    }

    private void privateMethod() {
        System.out.println("Private");
    }

    void nomodifier() {
        System.out.println("no modifier");
    }

    protected void protectedMethod() {
        System.out.println("protected");
    }

    public void publicMethod() {
        System.out.println("public");
    }

    public void testPrivate() {
        Student s11 = new Student();
        s11.protectedMethod();
    }


}
