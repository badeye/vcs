package main;

import java.util.Scanner;

public class ciklas1 {
    public static void main(String[] args) {
        for(int i=0; i<5; i++) System.out.println("Hello World!");


        System.out.println("Iveskite skaiciu");
        int number=readNumber();
        for(int i=0; i<number; i++) System.out.println("Hello World!");


        String nuo1iki50="";
        for(int i=1;i<51;i++) nuo1iki50+=(i+" ");
        System.out.println(nuo1iki50);

        int suma=0;
        for(int i=1; i<=100; i++)suma+=i;

        System.out.println("suma nuo 1 iki 100 yra " + suma + " (for ciklas)");

        suma=0;
        int index=100;



        while(index>0){
            suma+=index;
            index--;
        }





        System.out.println("suma nuo 1 iki 100 yra " + suma + " (while ciklas)");

        suma=0;


        do{

            suma+=++index;

        }while(index<100);




        System.out.println("suma nuo 1 iki 100 yra " + suma + " (do while ciklas)");
    }

    private static int readNumber(){
        Scanner scanner = new Scanner(System.in);
        int readNumber=scanner.nextInt();

        return readNumber;

    }

}
