package main;

import java.util.Scanner;

public class CiklasMasyvas {

    public static void main(String[] args) {

  //      consoleArray2();
        int array1[]={1,1,1,2,3,1,2,1,2,1,2};
        int array2[]={1,1,1,2,3,1,2,1,2,1,2};
        System.out.println(areEqual(array1,array2));

    }

    public static String printArray(int... array){
        String s="";
        for (int a: array)s+=(a+" ");


        return s;
    }



    public static int sumArray (int... array){
        int sum=0;
        for(int a: array) sum+=a;
        return sum;
    }

    public static int[] inverseArray (int... array){
        int l= array.length;
        int[] array2 = new int[l];
        for (int i=0; i<l; i++) array2[i]=array[l-i-1];
        return array2;
    }

    public static void consoleArray(){
        int[] array = new int[5];
        String s="";
        for (int i=0; i<5;i++){
            System.out.println("Iveskite "+ i + "masyvo elementa" );
            array[i]=readNumber();
            s+=array[i]+" ";
        }
        System.out.println("Ivestas masyvas:\n"+s);

    }


    public static void consoleArray2(){
        System.out.println("Iveskite masyvo ilgi");
        int l=readNumber();

        int[] array = new int[l];
        String s="";
        for (int i=0; i<l;i++){
            System.out.println("Iveskite "+ i + " masyvo elementa" );
            array[i]=readNumber();

        }
        System.out.println("Ivestas masyvas:\n"+printArray(array));
        System.out.println("Apverstas masyvas:\n" + inverseArrayString(array));
        System.out.println("Masyvo suma "+ sumArray(array));
        System.out.println("Masyvo vidurkis " + averArray(array));

    }

    public static double averArray(int... array){

        return 1.0*sumArray(array)/array.length;
    }


    public static int productArray(int... array){
        int prod=1;
        for (int a: array)prod*=a;
        return prod;
    }

    public static String inverseArrayString(int... array){
        int[] invArray=inverseArray(array);
        String s=printArray(invArray);
        return s;

    }

    public static boolean areEqual (int[] a, int[] b){

        int al=a.length;
        if (al!=b.length || al==0) return false;

        while(al>0){
            al--;
            if (a[al]!=b[al]) return false;
        }
        return true;
    }






    private static int readNumber(){
        Scanner scanner = new Scanner(System.in);
        int readNumber=scanner.nextInt();

        return readNumber;

    }
}
